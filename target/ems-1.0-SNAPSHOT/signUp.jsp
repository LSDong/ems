<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>员工管理系统-注册</title>
  <!-- Fav  Icon Link -->
  <link rel="shortcut icon" type="image/png" href="${pageContext.request.contextPath}/statics/images/fav.png">
  <!-- Bootstrap core CSS -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/bootstrap.min.css">
  <!-- themify icons CSS -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/themify-icons.css">
  <!-- Main CSS -->
  <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/styles.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/green.css" id="style_theme">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/responsive.css">

  <script src="${pageContext.request.contextPath}/statics/js/modernizr.min.js"></script>
</head>

<body class="auth-bg">
<!-- Pre Loader -->
<div class="loading">
  <div class="spinner">
    <div class="double-bounce1"></div>
    <div class="double-bounce2"></div>
  </div>
</div>
<!--/Pre Loader -->
<!-- Color Changer -->
<div class="theme-settings" id="switcher">
		<span class="theme-click">
			<span class="ti-settings"></span>
		</span>
  <span class="theme-color theme-default theme-active" data-color="green"></span>
  <span class="theme-color theme-blue" data-color="blue"></span>
  <span class="theme-color theme-red" data-color="red"></span>
  <span class="theme-color theme-violet" data-color="violet"></span>
  <span class="theme-color theme-yellow" data-color="yellow"></span>
</div>
<!-- /Color Changer -->
<div class="wrapper">
  <!-- Page Content -->
  <div id="content">
    <div class="container">
      <div class="row">
        <div class="col-sm-6 auth-box">
          <div class="lochana-box-shadow">
            <!-- Page Title -->
            <h3 class="widget-title">注册</h3>
            <!-- /Page Title -->

            <!-- Form -->
            <form class="widget-form" action="${pageContext.request.contextPath}/mng/register" method="post">

              <div class="form-group row">
                <div class="col-sm-12">
                  <input name="name" placeholder="请输入用户名" class="form-control" required="" data-validation="length alphanumeric" data-validation-length="3-12"
                         data-validation-error-msg="User name has to be an alphanumeric value (3-12 chars)" data-validation-has-keyup-event="true">
                </div>
              </div>

              <div class="form-group row">
                <div class="col-sm-12">
                  <input type="password" placeholder="请输入密码" name="password" class="form-control" data-validation="strength" data-validation-strength="2"
                         data-validation-has-keyup-event="true">
                </div>
              </div>

              <div class="form-group row">
                <div class="col-sm-12">
                  <input type="password" placeholder="再次确认密码" name="pass_confirmation" class="form-control" data-validation="strength"
                         data-validation-strength="2" data-validation-has-keyup-event="true">
                </div>
              </div>
              <div class="form-check row">
                <div class="col-sm-12 text-left">
                  <div class="custom-control custom-checkbox">
                    <input class="custom-control-input" type="checkbox" id="ex-check-2">
                    <label class="custom-control-label" for="ex-check-2">我同意许可协议</label>
                  </div>
                </div>
              </div>
              <!-- Button -->
              <div class="button-btn-block">
                <input type="submit" value="注册" class="btn btn-primary btn-lg btn-block"/>
              </div>
              <!-- /Button -->
              <!-- Linsk -->
              <div class="auth-footer-text">
                <small>有账号,在这里 <a href="${pageContext.request.contextPath}/login.jsp">登录</a> </small>
              </div>
              <!-- /Links -->
            </form>
            <!-- /Form -->
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /Page Content -->
</div>
<!-- Jquery Library-->
<script src="${pageContext.request.contextPath}/statics/js/jquery-3.2.1.min.js"></script>
<!-- Popper Library-->
<script src="${pageContext.request.contextPath}/statics/js/popper.min.js"></script>
<!-- Bootstrap Library-->
<script src="${pageContext.request.contextPath}/statics/js/bootstrap.min.js"></script>
<!-- Custom Script-->
<script src="${pageContext.request.contextPath}/statics/js/custom.js"></script>
</body>

</html>

<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>员工管理系统-错误页面</title>
    <!-- Fav  Icon Link -->
    <link rel="shortcut icon" type="image/png" href="${pageContext.request.contextPath}/statics/images/fav.png">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/bootstrap.min.css">
    <!-- themify icons CSS -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/themify-icons.css">
    <!-- Main CSS -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/green.css" id="style_theme">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/responsive.css">

    <script src="${pageContext.request.contextPath}/statics/js/modernizr.min.js"></script>
</head>

<body>
<!-- Pre Loader -->
<div class="loading">
    <div class="spinner">
        <div class="double-bounce1"></div>
        <div class="double-bounce2"></div>
    </div>
</div>
<!--/Pre Loader -->
<!-- Color Changer -->
<div class="theme-settings" id="switcher">
		<span class="theme-click">
			<span class="ti-settings"></span>
		</span>
    <span class="theme-color theme-default theme-active" data-color="green"></span>
    <span class="theme-color theme-blue" data-color="blue"></span>
    <span class="theme-color theme-red" data-color="red"></span>
    <span class="theme-color theme-violet" data-color="violet"></span>
    <span class="theme-color theme-yellow" data-color="yellow"></span>
</div>
<!-- /Color Changer -->
<div class="wrapper">
    <!-- Page Content -->
    <div id="content">
       <jsp:include page="/WEB-INF/jsp/header.jsp"/>
        <!-- Page Title -->
        <div class="container mt-0">
            <div class="row breadcrumb-bar">
                <div class="col-md-6">
                    <h3 class="block-title">错误页面</h3>
                </div>
                <div class="col-md-6">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="index.html">
                                <span class="ti-home"></span>
                            </a>
                        </li>
                        <li class="breadcrumb-item active">错误页面</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /Page Title -->
        <!-- /Breadcrumb -->
        <!-- Main Content -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="widget-area-2 lochana-box-shadow min-h200">
                        <h3 class="widget-title">${error}</h3>
                        <div class="text-center">
                            <span class="ti-unlink unlink"></span>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <!-- /Main Content -->
        <!--Copy Rights-->
       <jsp:include page="/WEB-INF/jsp/footer.jsp"/>
        <!-- /Copy Rights-->
    </div>
</div>
<!-- Back to Top -->
<a id="back-to-top" href="#" class="back-to-top">
    <span class="ti-angle-up"></span>
</a>
<!-- /Back to Top -->
<!-- Jquery Library-->
<script src="${pageContext.request.contextPath}/statics/js/jquery-3.2.1.min.js"></script>
<!-- Popper Library-->
<script src="${pageContext.request.contextPath}/statics/js/popper.min.js"></script>
<!-- Bootstrap Library-->
<script src="${pageContext.request.contextPath}/statics/js/bootstrap.min.js"></script>
<!-- Custom Script-->
<script src="${pageContext.request.contextPath}/statics/js/custom.js"></script>

</body>

</html>
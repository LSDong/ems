<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>员工管理系统-登录</title>
    <!-- Fav  Icon Link -->
    <link rel="shortcut icon" type="image/png" href="${pageContext.request.contextPath}/statics/images/fav.png">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/bootstrap.min.css">
    <!-- themify icons CSS -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/themify-icons.css">
    <!-- Main CSS -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/green.css" id="style_theme">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/responsive.css">

    <script src="${pageContext.request.contextPath}/statics/js/modernizr.min.js"></script>
</head>

<body class="auth-bg">
<!-- Pre Loader -->
<div class="loading">
    <div class="spinner">
        <div class="double-bounce1"></div>
        <div class="double-bounce2"></div>
    </div>
</div>
<!--/Pre Loader -->
<!-- Color Changer -->
<div class="theme-settings" id="switcher">
		<span class="theme-click">
			<span class="ti-settings"></span>
		</span>
    <span class="theme-color theme-default theme-active" data-color="green"></span>
    <span class="theme-color theme-blue" data-color="blue"></span>
    <span class="theme-color theme-red" data-color="red"></span>
    <span class="theme-color theme-violet" data-color="violet"></span>
    <span class="theme-color theme-yellow" data-color="yellow"></span>
</div>
<!-- /Color Changer -->
<div class="wrapper">
    <!-- Page Content  -->
    <div id="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 auth-box">
                    <div class="lochana-box-shadow">
                        <h3 class="widget-title">登录</h3>
                        <form class="widget-form" action="${pageContext.request.contextPath}/mng/login" method="post">
                            <!-- form-group -->
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <input name="name" placeholder="请输入用户名" class="form-control" required=""
                                           data-validation="length alphanumeric" data-validation-length="3-12"
                                           data-validation-error-msg="User name has to be an alphanumeric value (3-12 chars)"
                                           data-validation-has-keyup-event="true">
                                </div>
                            </div>
                            <!-- /.form-group -->
                            <!-- form-group -->
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <input type="password" placeholder="请输入密码" name="password" class="form-control"
                                           data-validation="strength" data-validation-strength="2"
                                           data-validation-has-keyup-event="true">
                                </div>
                            </div>
                            <!-- /.form-group -->
                            <!-- form-group -->
                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <input type="text" placeholder="请输入验证码" name="code" class="form-control"
                                           data-validation="strength" data-validation-strength="2"
                                           data-validation-has-keyup-event="true">
                                </div>
                            </div>
                            <!-- /.form-group -->
                            <!-- form-group -->
                            <div class="form-group row">
                                <div class="col-sm-6 text-left">
                                    <img src="${pageContext.request.contextPath}/captcha" id="vCode">
                                </div>
                                <div class="col-sm-6 text-right">
                                    <span class="text-danger">${requestScope.error}</span>
                                </div>
                            </div>
                            <!-- /.form-group -->
                            <!-- Check Box -->
                            <div class="form-check row">
                                <div class="col-sm-12 text-left">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" id="ex-check-2">
                                        <label class="custom-control-label" for="ex-check-2">记住我</label>
                                    </div>
                                </div>
                            </div>
                            <!-- /Check Box -->
                            <!-- Login Button -->
                            <div class="button-btn-block">
                                <input type="submit" value="登录"  class="btn btn-primary btn-lg btn-block">
                            </div>
                            <!-- /Login Button -->
                            <!-- Links -->
                            <div class="auth-footer-text">
                                <small>新用户,在这里 <a href="${pageContext.request.contextPath}/signUp.jsp">注册</a> </small>
                            </div>
                            <!-- /Links -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Page Content  -->
</div>
<!-- Jquery Library-->
<script src="${pageContext.request.contextPath}/statics/js/jquery-3.2.1.min.js"></script>
<!-- Popper Library-->
<script src="${pageContext.request.contextPath}/statics/js/popper.min.js"></script>
<!-- Bootstrap Library-->
<script src="${pageContext.request.contextPath}/statics/js/bootstrap.min.js"></script>
<!-- Custom Script-->
<script src="${pageContext.request.contextPath}/statics/js/custom.js"></script>

<script>
    $(function () {
        $("#vCode").click(function () {
            // 刷新验证码
            let src = $("#vCode").attr("src");
            $("#vCode").attr("src",src+"?v="+new Date().getTime());
        });
    });
</script>
</body>

</html>
package com.qf.ems.dao;

import com.qf.ems.entity.Manager;

public interface ManagerMapper {

    int insert(Manager manager);

    Manager selectByName(String name);
}

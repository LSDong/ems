package com.qf.ems.dao;

import com.qf.ems.entity.Emp;

import java.util.List;

public interface EmpMapper {

    List<Emp> selectAll(String name);


    int insert(Emp emp);

    int update(Emp emp);

    int delete(Integer id);


    Emp selectById(Integer id);



}

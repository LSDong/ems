package com.qf.ems.service;

import com.github.pagehelper.PageInfo;
import com.qf.ems.entity.Emp;

public interface EmpService {

    PageInfo<Emp> selectByPage(Integer page,Integer size,String name);

    Emp selectOne(Integer id);

    Boolean insert(Emp emp);

    Boolean update(Emp emp);

    Boolean deleteById(Integer id);
}

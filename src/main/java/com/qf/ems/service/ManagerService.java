package com.qf.ems.service;

import com.qf.ems.entity.Manager;

public interface ManagerService {


    /**
     * 登录
     */
    Manager login(String name, String password);


    /**
     * 注册
     */
    Boolean register(Manager manager);


    /**
     * 查询用户是否存在
     */
    Boolean selectByName(String name);
}

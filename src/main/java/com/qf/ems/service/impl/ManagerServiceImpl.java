package com.qf.ems.service.impl;

import com.qf.ems.dao.ManagerMapper;
import com.qf.ems.entity.Manager;
import com.qf.ems.service.ManagerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;


@Service
@Slf4j
@RequiredArgsConstructor
@Transactional(rollbackFor = Exception.class)
public class ManagerServiceImpl implements ManagerService {

    final ManagerMapper managerMapper;

    @Override
    public Manager login(String name, String password) {
        log.info("{}开始登陆"+name);

        Manager manager = managerMapper.selectByName(name);
        if(manager.getPassword().equals(password)){
            return manager;

        }
        return null;
    }

    @Override
    public Boolean register(Manager manager) {
        return managerMapper.insert(manager)>0;
    }

    @Override
    public Boolean selectByName(String name) {
        Manager manager = managerMapper.selectByName(name);
        return StringUtils.isEmpty(manager);
    }
}

package com.qf.ems.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.qf.ems.dao.EmpMapper;
import com.qf.ems.entity.Emp;
import com.qf.ems.service.EmpService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;


@Service
@Slf4j
@RequiredArgsConstructor
@Transactional(rollbackFor = Exception.class)
public class EmpServiceImpl implements EmpService {

    final EmpMapper empMapper;

    @Override
    public PageInfo<Emp> selectByPage(Integer page, Integer size, String name) {
        PageHelper.startPage(page,size);
        List<Emp> list = empMapper.selectAll(name);
        PageInfo<Emp> info = new PageInfo<>(list);

        return info;
    }

    @Override
    public Emp selectOne(Integer id) {
        return empMapper.selectById(id);
    }

    @Override
    public Boolean insert(Emp emp) {
        //补全属性
        emp.setVisited(new Date());
        return empMapper.insert(emp)>0;
    }

    @Override
    public Boolean update(Emp emp) {
        emp.setVisited(new Date());
        return empMapper.update(emp)>0;
    }

    @Override
    public Boolean deleteById(Integer id) {
        return empMapper.delete(id)>0;
    }
}

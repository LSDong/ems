package com.qf.ems.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 经理
 *
 * @author lenovo
 * @date 2023/05/17
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Manager {
    private Integer id;
    private String name;
    private String password;
}

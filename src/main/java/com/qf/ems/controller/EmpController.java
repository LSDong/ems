package com.qf.ems.controller;

import com.github.pagehelper.PageInfo;
import com.qf.ems.entity.Emp;
import com.qf.ems.service.EmpService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("emp")
@RequiredArgsConstructor
public class EmpController {

    final EmpService empService;

    @RequestMapping("page")
    public String page(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                       @RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
                       String name, HttpServletRequest request){
        PageInfo<Emp> pageInfo = empService.selectByPage(page, size, name);
        request.setAttribute("pageInfo",pageInfo);
        request.setAttribute("name",name);
        return "/WEB-INF/jsp/showEmp";

    }

    @GetMapping("one")
    public String selectOne(Integer id,HttpServletRequest request){
        Emp emp = empService.selectOne(id);
        request.setAttribute("emp",emp);
        return "/WEB-INF/jsp/updateEmp";
    }

    @GetMapping("toAdd")
    public String toAdd(){
        return "/WEB-INF/jsp/addEmp";
    }

    @PostMapping("add")
    public String add(Emp emp){
        empService.insert(emp);
        return "redirect:/emp/page";
    }

    @GetMapping("toEdit")
    public String toEdit(Integer id,HttpServletRequest request){
        Emp emp = empService.selectOne(id);
        request.setAttribute("emp",emp);
        return "/WEB-INF/jsp/updateEmp";
    }

    @PostMapping("update")
    public String update(Emp emp){
        Boolean update = empService.update(emp);
        return "redirect:/emp/page";
    }

    @GetMapping("delete")
    public String delete(Integer id){
        empService.deleteById(id);
        return "redirect:/emp/page";
    }
}
